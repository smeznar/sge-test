import argparse
from utils import load_embedder


def evaluate_embedder(args):
    for i in range(1):
        for emb in args.embedders:
            embedder_class = load_embedder(emb)
            embedder = embedder_class()
            #embedder.optimization_params = {"num_of_features": 2**i}
            print("Embedder found, evaluation starting.")
            print(embedder.evaluate(args))
            print('Evaluation has been completed successfully.')


def main():
    parser = argparse.ArgumentParser(description='Graph Embedding Evaluation Utility')
    parser.add_argument('--embedders', help='Embedder identifier', required=True, action='store', nargs='*')
    parser.add_argument('--datasets', help='Names of datasets to be tested. (Bitcoin, Bitcoin_alpha, Blogspot,'
                                           ' citeseer, cora, Homo_sapiens, ions, POS)',
                        required=True, action='store', nargs='*')
    parser.add_argument('--save', help='Save embedding to a file', default=False, action='store_true')
    parser.add_argument("--num-shuffles", default=10, type=int, help='Number of shuffles.')
    parser.add_argument("--all", default=False, action='store_true',
                        help='The embeddings are evaluated on all training percents from 10 to 90 when this flag is set to true. '
                             'By default, only training percents of 10, 50 and 90 are used.')
    parser.add_argument("--data_perc", default=0.5, type=float, help='data percentage if all is not chosen.')
    args = parser.parse_args()

    evaluate_embedder(args)


if __name__ == "__main__":
    main()
