import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import argparse
import json
import glob
from sklearn.metrics import auc
from matplotlib import rc, font_manager

# font_size = 30 # time
font_size = 10 # micro/macro
font_properties = {'family': 'serif', 'serif': ['Computer Modern Roman'],
                   'weight': 'normal', 'size': font_size}

font_manager.FontProperties(family='Computer Modern Roman', style='normal',
                            size=font_size, weight='normal', stretch='normal')


# colours = ["#264653", "#287271", "#2a9d8f", "#8ab17d","#e9c46a","#efb366","#f4a261","#ee8959","#e76f51"]
colours = ["#DF2A2D", "#f94144", "#f3722c", "#f8961e", "#f9c74f", "#90be6d", "#43aa8b", "#577590", "#455D73"]


# metrics_names = ["Random", "LP", "vGCN", "Personalized pagerank", "line", "node2vec", "deepwalk", "Netmf", "SNoRe", "SNoReBin"]
# mt_names = ["Random", "LP", "VGAE", "PPRS", "LINE", "Node2Vec", "Deepwalk", "NetMF (SCD)", "SNoRe", "SNoRe SDF"]

dataset_names = ["Bitcoin", "Bitcoin_alpha", "co_auth_cs", "blogcatalog", "citeseer", "co_auth_phy", "cora",
                 "Homo_sapiens", "ions", "POS", "pubmed"]
dt_names = ["Bitcoin", "Bitcoin Alpha", "Coauthor CS", "BlogCatalog", "Citeseer", "Coauthor PHY", "Cora",
                 "Homo sapiens (PPI)", "Ions", "Wikipedia", "Pubmed"]

# metrics_names = ["SNoRe-EUC", "SNoRe-SEUC", "SNoRe-CAN", "SNoRe-HAM", "SNoRe-JAC", "SNoRe", "SNoRe_fni"]
# mt_names = ["Euclidean", "Standardized Euclidean", "Canberra", "Hamming", "Jaccard", "Cosine", "Cosine SDF"]

metrics_names = ["SNoReBin128", "SNoReSDF"]
mt_names = ["Digitized", "Not digitized"]


def plot_micro(filenames):
    datasets = []
    perc = []
    values = []
    setting = []
    enum = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r') as file:
            res_file = json.load(file)
            name = res_file['name']
            ind = metrics_names.index(name)
            name = mt_names[ind]
            for r in res_file['results']:
                for s in r['scores'].keys():
                    total = 0
                    for v in r['scores'][s]:
                        total += v['micro']
                    dt_index = dataset_names.index(r["dataset"])
                    datasets.append(dt_names[dt_index])
                    perc.append(float(s))
                    values.append(total/len(r['scores'][s]))
                    setting.append(name)
                    enum.append(ind)
    df = pd.DataFrame(data={'Dataset': datasets, 'Percent train': perc,
                            'Micro F1': values, 'Setting': setting, "enum": enum})
    df = df.groupby(["Setting", "Dataset", "Percent train"], as_index=False).mean()
    df = df.sort_values(["enum", "Dataset", "Percent train"])
    grid = sns.FacetGrid(df, col="Dataset", hue='Setting', col_wrap=4)
    grid.map(plt.plot, "Percent train", "Micro F1", marker="o").add_legend(loc=4, fontsize='xx-large')
    grid.savefig("micro_plot", quality=95, dpi=300)
    #plt.savefig("micro_plot", quality=95, dpi=600)


def plot_macro(filenames):
    datasets = []
    perc = []
    values = []
    setting = []
    enum = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r') as file:
            res_file = json.load(file)
            name = res_file['name']
            ind = metrics_names.index(name)
            name = mt_names[ind]
            for r in res_file['results']:
                for s in r['scores'].keys():
                    total = 0
                    for v in r['scores'][s]:
                        total += v['macro']
                    dt_index = dataset_names.index(r["dataset"])
                    datasets.append(dt_names[dt_index])
                    perc.append(float(s))
                    values.append(total/len(r['scores'][s]))
                    setting.append(name)
                    enum.append(ind)
    df = pd.DataFrame(data={'Dataset': datasets, 'Percent train': perc,
                            'Macro F1': values, 'Setting': setting, "enum": enum})
    df = df.groupby(["Setting", "Dataset", "Percent train"], as_index=False).mean()
    df = df.sort_values(["enum", "Dataset", "Percent train"])
    grid = sns.FacetGrid(df, col="Dataset", hue='Setting', col_wrap=4)
    grid.map(plt.plot, "Percent train", "Macro F1", marker="o").add_legend(loc=4, fontsize='xx-large')
    grid.savefig("macro_plot", quality=95, dpi=300)


def plot_time(filenames):
    datasets = []
    time = []
    setting = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r') as file:
            res_file = json.load(file)
            name = mt_names[metrics_names.index(res_file['name'])]
            if name != "Random" and name != "LP":
                for r in res_file['results']:
                    dt_index = dataset_names.index(r["dataset"])
                    datasets.append(dt_names[dt_index])
                    time.append(r['time'])
                    setting.append(name)
    px = sns.barplot(x="Time", y="Dataset", hue="Setting", errwidth=2, capsize=0.1, data={"Dataset": datasets, "Setting": setting,
                                                         "Time": time}, orient='h')
    plt.legend(fontsize="large", loc=4)
    plt.yticks(fontsize="medium")
    plt.xticks(fontsize="large")
    # px.figure.set_fontsize("medium")
    # plt.show()
    px.figure.set_size_inches((30, 20))
    px.figure.savefig("time_plot", quality=95, dpi=300)
    a = a/0


def aggregate_results_latex_table(filenames):
    datasets = []
    perc = []
    values = []
    values_mi = []
    setting = []
    enum = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r') as file:
            res_file = json.load(file)
            name = res_file['name']
            ind = metrics_names.index(name)
            name = mt_names[ind]
            for r in res_file['results']:
                for s in r['scores'].keys():
                    total = 0
                    total_mi = 0
                    for v in r['scores'][s]:
                        total += v['macro']
                        total_mi += v["micro"]
                    dt_index = dataset_names.index(r["dataset"])
                    datasets.append(dt_names[dt_index])
                    perc.append(float(s))
                    values.append(total/len(r['scores'][s]))
                    values_mi.append(total_mi/len(r["scores"][s]))
                    setting.append(name)
                    enum.append(ind)
    df = pd.DataFrame(data={'dataset': datasets, 'percent_train': perc, "enum": enum,
                            'macro F1': values, 'micro F1': values_mi, 'setting': setting})
    # df["dataset"] = df["dataset"].str.lower()
    stds = df.groupby(["setting", "dataset"]).std()
    df = df.groupby(["setting", "dataset"]).mean().drop("percent_train", 1)
    df = df.assign(micro_std=stds["micro F1"], macro_std=stds["macro F1"])
    stds["macro F1"] = stds["macro F1"].round(decimals=3).astype(str)
    stds["micro F1"] = stds["micro F1"].round(decimals=3).astype(str)
    df["macro F1"] = df["macro F1"].round(decimals=3).astype(str)
    df["micro F1"] = df["micro F1"].round(decimals=3).astype(str)
    df["macro F1"] = df["macro F1"] + " (pm" + stds["macro F1"] + ")"
    df["micro F1"] = df["micro F1"] + " (pm" + stds["micro F1"] + ")"
    df = df.sort_values(["enum", "dataset"])
    df = df.drop(columns="enum")
    df.drop(columns=["micro F1"]).pivot_table(columns="setting", index="dataset", values="macro F1", aggfunc="first").reindex(columns=mt_names).to_latex("macro_aggregate.tex", index=True)
    df.drop(columns="macro F1").pivot_table(columns="setting", index="dataset", values="micro F1", aggfunc="first").reindex(columns=mt_names).to_latex("micro_aggregate.tex", index=True)


def plot_micro_macro_features(filenames):
    datasets = []
    perc = []
    values_mi = []
    values_ma = []
    setting = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r') as file:
            res_file = json.load(file)
            for r in res_file['results']:
                for s in r['scores'].keys():
                    total = 0
                    total_ma = 0
                    for v in r['scores'][s]:
                        total += v['micro']
                        total_ma += v['macro']
                    dt_index = dataset_names.index(r["dataset"])
                    datasets.append(dt_names[dt_index])
                    perc.append(float(s))
                    values_mi.append(total/len(r['scores'][s]))
                    values_ma.append(total_ma/len(r['scores'][s]))
                    setting.append(r["parameters"]['num_of_features'])
    df = pd.DataFrame(data={'Dataset': datasets, 'Percent train': perc,
                            'Micro F1': values_mi, 'Setting': setting, "Macro F1": values_ma})
    df = df.groupby(["Setting", "Dataset", "Percent train"], as_index=False).mean()
    df = df.sort_values(["Setting", "Dataset", "Percent train"])
    slot = df[['Setting', 'Dataset']].drop_duplicates()
    setting = []
    area = []
    dataset = []
    perc = [(i+1)/10 for i in range(9)]
    for i, r in slot.iterrows():
        c = df[(df["Setting"]==r["Setting"]) & (df["Dataset"]==r["Dataset"])]
        c.sort_values(["Percent train"])
        setting.append(r["Setting"])
        dataset.append(r["Dataset"])
        area.append(auc(perc, c["Macro F1"]))
    df = pd.DataFrame(data={"Setting": setting, "Dataset": dataset, "AUC": area})
    for d in df["Dataset"].unique():
        dsr = df["Dataset"]==d
        df.loc[dsr, "AUC"] = (df[dsr]["AUC"] - df[dsr]["AUC"].min())/(df[dsr]["AUC"].max()-df[dsr]["AUC"].min())
    df = df.pivot("Dataset", "Setting", "AUC")
    hm = sns.heatmap(df, cmap="coolwarm", linewidths=.5)
    # grid = sns.FacetGrid(df, col="Dataset", hue='Setting', col_wrap=4)
    # grid.map(plt.plot, "Percent train", "Macro F1", marker="o").add_legend()
    # plt.show()
    hm.figure.set_size_inches((16, 12))
    hm.figure.savefig("macro_plot_features", quality=95, dpi=600)
    # grid = sns.FacetGrid(df, col="Dataset", hue='Setting', col_wrap=4)
    # grid.map(plt.plot, "Percent train", "Micro F1", marker="o").add_legend()
    # plt.show()
    # grid.savefig("micro_plot", quality=95, dpi=600)


def plot_time_nf(filenames):
    datasets = []
    time = []
    setting = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r') as file:
            res_file = json.load(file)
            #name = bs_name[baseline_names.index(res_file['name'])]
            #if name != "Random" and name != "LP":
            for r in res_file['results']:
                dt_index = dataset_names.index(r["dataset"])
                datasets.append(dt_names[dt_index])
                time.append(r['time'])
                setting.append(r['parameters']['num_of_features'])
    px = sns.barplot(x="Time", y="Dataset", hue="Setting", errwidth=2, capsize=0.1,
                     data={"Dataset": datasets, "Setting": setting, "Time": time}, orient='h')
    plt.legend(fontsize="large", loc=4)
    plt.yticks(fontsize="medium")
    plt.xticks(fontsize="large")
    # px.figure.set_fontsize("medium")
    # plt.show()
    px.figure.set_size_inches((30, 20))
    px.figure.savefig("time_plot", quality=95, dpi=300)
    a = a/0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--res_files", help='File with results for an embedder',
                        required=True, action='store', nargs='*')
    args = parser.parse_args()
    files = glob.glob(args.res_files[0])
    # aggregate_results_latex_table(files)
    # sns.set()
    sns.set(font_scale=1.60, style="whitegrid", palette=sns.color_palette("Set2"))  # micro/macro
    # sns.set(font_scale=1.4, style="whitegrid", palette=sns.color_palette("Set2")) # time
    # sns.set_style("whitegrid")
    rc('text', usetex=True)
    rc('font', **font_properties)


    # PLOTS FOR COMPARISON BETWEEN DIFFERENT BASELINES
    # plot_time(files)
    plot_micro(files)
    plot_macro(files)

    # PLOTS FOR NUMBER OF FEATURES
    # plot_time_nf(args.res_files)
    # plot_micro_macro_features(args.res_files)

