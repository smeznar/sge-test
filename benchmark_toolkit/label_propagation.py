## this is the basic label propagation on the graph-adjacency matrix

## simple label propagation algorithm

## label propagation algorithms:
import networkx as nx
import numpy as np
import scipy.sparse as sp
import time
import os
import sys
import json
from collections import defaultdict
import multiprocessing as mp  ## initialize the MP part
import mkl


def label_propagation_normalization(matrix):
    matrix = matrix.tocsr()
    try:
        matrix.setdiag(0)
    except TypeError:
        matrix.setdiag(np.zeros(matrix.shape[0]))
    d = matrix.sum(axis=1).getA1()
    nzs = np.where(d > 0)
    d[nzs] = 1 / np.sqrt(d[nzs])
    dm = sp.diags(d, 0).tocsc()
    return dm.dot(matrix).dot(dm)


def pr_kernel(index_row):
    pr = sparse_page_rank(graph, [index_row],
                          epsilon=1e-6,
                          max_steps=100000,
                          try_shrink=True)

    norm = np.linalg.norm(pr, 2)
    if norm > 0:
        pr = pr / np.linalg.norm(pr, 2)
        return (index_row, pr)
    else:
        return (index_row, np.zeros(graph.shape[1]))


## dodaj numba compiler tule
def label_propagation(graph_matrix, class_matrix, alpha, epsilon=1e-12, max_steps=100000, weight_mode=None):
    if weight_mode == "colnorm":
        i = 1. / class_matrix.sum(axis=0)
        i[~np.isfinite(i)] = 0
        class_matrix *= i.T
    #        class_matrix = np.multiply(class_matrix,i)

    diff = np.inf
    steps = 0
    current_labels = class_matrix
    while diff > epsilon and steps < max_steps:
        steps += 1
        new_labels = alpha * graph_matrix.dot(current_labels) + (1 - alpha) * class_matrix
        diff = np.linalg.norm(new_labels - current_labels) / np.linalg.norm(new_labels)
        current_labels = new_labels

    return current_labels


def write_results(dataset_name, results, evaluation_time):
    filepath = os.path.join(os.path.dirname(sys.argv[0]), 'results', "LP_results.json")
    if os.path.exists(filepath):
        with open(filepath, "r") as file:
            json_file = json.load(file)
    else:
        json_file = {"name": "LP", "results": []}
    json_file["results"].append({"dataset": dataset_name, "parameters": {"alpha": 0.9},
                                 "notes": "Label propagation algorithm", "time": evaluation_time,
                                 "scores": results})
    with open(filepath, 'w') as file:
        json.dump(json_file, file)


if __name__ == "__main__":
    import argparse
    import scipy.io
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import f1_score
    from sklearn.model_selection import ShuffleSplit
    from scipy import sparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--datasets', help='Names of datasets to be tested. (Bitcoin, Bitcoin_alpha, Blogspot,'
                                           ' citeseer, cora, Homo_sapiens, ions, POS)',
                        required=True, action='store', nargs='*')
    parser.add_argument("--out_results", default="../results/testLP.txt")
    parser.add_argument("--sample_size", default=0.1)
    parser.add_argument("--weight_scheme", default="")
    parser.add_argument("--alpha", default=0.90)
    args = parser.parse_args()
    for d in args.datasets:
        print("Loading..")
        mat = scipy.io.loadmat("/Users/smeznar/IJS/sge-test/benchmark_toolkit/datasets/"+d+".mat")
        labels = mat['group']

        try:
            labels = labels.todense()
        except:
            pass

        core_network = mat['network']
        matrix = label_propagation_normalization(core_network)
        print("Propagation..")
        all_results = defaultdict(list)
        avg_time = []
        for j in np.arange(0.1, 1, 0.1):
            print("Train size:{}".format(j))
            rs = ShuffleSplit(n_splits=10, test_size=j, random_state=0)
            for X_test, X_train in rs.split(labels):
                start = time.time()
                tmp_labels = labels.copy()
                true_labels = tmp_labels[X_test].copy()
                tmp_labels[X_test] = 0
                probs = label_propagation(matrix, tmp_labels, args.alpha, weight_mode=args.weight_scheme)
                all_labels = []
                y_test = [[] for _ in range(labels.shape[0])]
                cy = sparse.csr_matrix(labels).tocoo()
                for i, b in zip(cy.row, cy.col):
                    y_test[i].append(b)
                top_k_list = [len(l) for l in y_test]
                assert labels.shape[0] == len(top_k_list)
                predictions = []
                for i, k in enumerate(top_k_list):
                    probs_ = probs[i, :]
                    a = np.zeros(probs.shape[1])
                    labels_tmp = probs_.argsort()[-k:]
                    a[labels_tmp] = 1
                    predictions.append(a)

                predicted_labels = np.matrix(predictions)[X_test]
                micro = f1_score(true_labels, predicted_labels, average='micro')
                macro = f1_score(true_labels, predicted_labels, average='macro')
                end = time.time()
                elapsed = end - start
                avg_time.append(elapsed)
                outarray = [str(j), str(micro), str(macro), "LP", d, str(elapsed)]
                print(outarray)
                all_results[j].append({"micro": micro, "macro": macro})
                # results.append(outarray)
        write_results(d, all_results, sum(avg_time) / len(avg_time))
