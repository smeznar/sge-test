from abc import abstractmethod, ABC
import os
import json
import sys
import numpy as np
import time
from interruptingcow import timeout
from sklearn.utils import shuffle as skshuffle
from sklearn.metrics import f1_score, accuracy_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.ensemble import RandomForestClassifier
from scipy.sparse import save_npz, issparse
from collections import defaultdict
from six import iteritems

from utils import read_graph, TopKRanker


class Embedder(ABC):
    def __init__(self):
        self.params = dict()
        self.optimization_params = dict()

    def name(self):
        return ""

    def notes(self):
        return ""

    def change_params(self):
        self.params.update(self.optimization_params)

    @abstractmethod
    def initialize(self, graph, labels):
        pass

    @abstractmethod
    def embed(self):
        pass

    def evaluate(self, params, param_opt=False):
        # params: datasets, num_shuffles, all, data_perc
        calculated_score = 0
        for d in params.datasets:
            all_results = defaultdict(list)
            graph_parts = read_graph(d)
            if graph_parts is None:
                continue
            else:
                network, labels, mlb = graph_parts
            start_time = time.time()
            try:
                with timeout(1*60*60, exception=TimeoutError):
                    self.initialize(network, labels)
                    if param_opt:
                        self.change_params()
                    emb = self.embed()
            except TimeoutError:
                print("embedding max time exceeded")
                emb = np.zeros((network.shape[0], 1))
            evaluation_time = time.time() - start_time
            print(evaluation_time)
            shuffles = []
            accuracy = 0
            for x in range(params.num_shuffles):
                shuffles.append(skshuffle(emb, labels))

            if params.all:
                training_percents = np.asarray(range(1, 10)) * .1
            else:
                training_percents = [params.data_perc]
            for train_percent in training_percents:
                for shuf in shuffles:
                    X, y = shuf

                    training_size = int(train_percent * X.shape[0])

                    X_train = X[:training_size, :]
                    y_train_ = y[:training_size]
                    # print(X_train.shape, y_train_.shape)
                    y_train = [list() for x in range(y_train_.shape[0])]

                    cy = y_train_.tocoo()
                    for i, j in zip(cy.row, cy.col):
                        y_train[i].append(j)

                    assert sum(len(l) for l in y_train) == y_train_.nnz

                    X_test = X[training_size:, :]
                    y_test_ = y[training_size:]

                    y_test = [[] for _ in range(y_test_.shape[0])]

                    cy = y_test_.tocoo()
                    for i, j in zip(cy.row, cy.col):
                        y_test[i].append(j)

                    clf = TopKRanker(LogisticRegression())
                    clf.fit(X_train, y_train_)

                    # find out how many labels should be predicted
                    top_k_list = [len(l) for l in y_test]
                    preds = clf.predict(X_test, top_k_list)


                    results = {}
                    averages = ["micro", "macro"]
                    for average in averages:
                        results[average] = f1_score(mlb.fit_transform(y_test),
                                                    mlb.fit_transform(preds), average=average)
                    accuracy += accuracy_score(mlb.fit_transform(y_test), mlb.fit_transform(preds))

                    all_results[train_percent].append(results)
            self.write_results(d, all_results, evaluation_time)
            print('Results of ', self.name(), 'on the dataset', d,
                  'using embeddings of dimensionality', emb.shape[1])
            print('-------------------')
            for train_percent in sorted(all_results.keys()):
                print('Train percent:', train_percent)
                # for index, result in enumerate(all_results[train_percent]):
                #     print('Shuffle #%d:   ' % (index + 1), result)
                avg_score = defaultdict(float)
                for score_dict in all_results[train_percent]:
                    for metric, score in iteritems(score_dict):
                        avg_score[metric] += score
                for metric in avg_score:
                    avg_score[metric] /= len(all_results[train_percent])
                print('Average score:', dict(avg_score))
                print('Average accuracy', accuracy/(len(training_percents)*params.num_shuffles))
                print('-------------------')
                calculated_score += avg_score['micro']
                calculated_score += avg_score['macro']

            if params.save:
                filepath = os.path.join(os.path.dirname(sys.argv[0]), 'embeddings',
                                        self.name() + "_" + d + "_emb.csv")
                if issparse(emb):
                    save_npz(filepath, emb)
                else:
                    np.save(filepath, emb)
        return calculated_score

    def write_results(self, dataset_name, results, evaluation_time):
        filepath = os.path.join(os.path.dirname(sys.argv[0]), 'results', self.name()+"_results.json")
        if os.path.exists(filepath):
            with open(filepath, "r") as file:
                json_file = json.load(file)
        else:
            json_file = {"name": self.name(), "results": []}
        json_file["results"].append({"dataset": dataset_name, "parameters": self.params,
                                     "notes": self.notes(), "time": evaluation_time,
                                     "scores": results})
        with open(filepath, 'w') as file:
            json.dump(json_file, file)
