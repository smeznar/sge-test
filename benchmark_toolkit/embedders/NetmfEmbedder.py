import sys

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from SCD.SCD import SCD_obj


class NMFEmbedder(Embedder):
    def name(self):
        return 'Netmf'

    def notes(self):
        return "Netmf embedder"

    def initialize(self, network, labels):
        self.params.update(NMFParams)
        node_names = list(range(network.shape[1]))
        self.network = network
        self.obj = SCD_obj(network, node_names=node_names)

    def embed(self):
        return self.obj.netMF_large(self.obj.input_graph, rank=self.params["rank"],
                                    embedding_dimension=self.params["embedding_dimension"],
                                    window=self.params["window"],
                                    negative=self.params["negative"])


NMFParams = {"rank": 256,
             "embedding_dimension": 128,
             "window": 10,
             "negative": 1.0}
