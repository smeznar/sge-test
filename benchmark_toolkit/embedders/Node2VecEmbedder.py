import sys
import numpy as np
from networkx import from_scipy_sparse_matrix

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from node2vec.src.main import main


class Node2VecEmbedder(Embedder):
    def name(self):
        return 'node2vec'

    def notes(self):
        return "embedder for the node2vec algorithem (Aditya Grover implementation)"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(N2VEParams)
        self.graph = from_scipy_sparse_matrix(graph)

    def embed(self):
        model = main(self.graph, self.params)
        return np.array([model.wv[str(node)] for node in self.graph.nodes])


N2VEParams = {"dimensions": 128,  # Number of dimensions (features)
            "walk_length": 80,  # Length of the walk per source
            "num_walks": 10,  # Number of walks per source
            "window_size": 10,  # Context size for optimization
            "iter": 1,  # Number of epochs in SGD
            "workers": 8,  # Number of workers
            "p": 1,  # Return hyper-parameter
            "q": 1,  # Input hyper-parameter
            "is_weighted": False,  # Is graph weighted
            "is_directed": False}  # Is graph directed
