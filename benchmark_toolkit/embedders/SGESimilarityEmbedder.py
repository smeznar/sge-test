import numpy as np
import time
import math
import tqdm
import sys
import networkx as nx
from collections import Counter
from itertools import chain
from Constants import THIS_LIB_SRC_FOLDER
sys.path.append(THIS_LIB_SRC_FOLDER)

from embedder import Embedder
from SGESim import SGESimilarity


class SGESimilarityEmbedder(Embedder):
    def name(self):
        return 'SGESimilarity'

    def notes(self):
        return "SGE random walk embedder based on the cosine similarity"

    def initialize(self, network, labels):
        # Update the params dictionary with significant parameters
        self.params.update(SGESParams)
        self.network = network
        self.labels = labels
        self.model = SGESimilarity(self.params["num_of_features"], feat_thresh=self.params["feature_inclusion_threshold"],
                                   num_walks=self.params["num_walks"], steps_start=self.params["steps_start"],
                                   steps_stop=self.params["steps_stop"], steps_step=self.params["steps_step"],
                                   node_appender=self.params["node_appender"],
                                   walk_appender=self.params["walk_appender"], distribution=self.params["distribution"])
        self.model.fit(self.network)
        # walk_generator = RandomWalkGenerator(self.network, self.labels, self.params)
        # self.walks = walk_generator.sample()
        # self.step_weight = lambda s: 1#1/(s+1)  # weight function for a step inside a walk
        # # input: step od the walk, output: step score
        # self.node_feature_weight = lambda s: s  # weight function for feature inside a node
        # # input: feature score between 0 and 1, output: re-weighted feature score
        # self.feature_representation_weight = feature_represtentation_all_same
        # # weight function for features based of representation between nodes
        # # input: Counter of , number of nodes
        # # output: dictionary of weights for node {node: weight}
        # nx_network = nx.from_scipy_sparse_matrix(network)
        # con_comp = list(nx.connected_components(nx_network))
        # enumerated_nodes = []
        # for con in con_comp:
        #     enumerated_nodes += [int(c) for c in nx.algorithms.traversal.dfs_tree(nx_network, list(con)[0])]
        # distance = len(enumerated_nodes)/self.params["num_of_features"]
        # indexes = [round(distance*i) for i in range(self.params["num_of_features"])]
        # self.feature_select = lambda d: [enumerated_nodes[i] for i in indexes]
        # #self.feature_select = lambda d: np.random.permutation(range(len(d)))[:self.params["num_of_features"]]
        # # feature selection function,
        # # input: list of dictionaries representing feature weight for each node {node_num: weight}
        # # output: numbers of nodes selected selected as features

    def embed(self):
        return self.model.transform(self.network, similarity_type=self.params["similarity_type"])
        # print("Walk scoring")
        # time.sleep(0.1)
        # dictionaries = []
        # for wa in tqdm.tqdm(self.walks):
        #     dictionaries.append(VecDict(wa, self.step_weight, self.node_feature_weight,
        #                                 self.params["feature_inclusion_threshold"]))
        # print("Reweight")
        # time.sleep(0.1)
        # node_freq = Counter(chain.from_iterable([d.document_terms() for d in dictionaries]))
        # node_weight = self.feature_representation_weight(node_freq, len(self.walks))
        # for d in tqdm.tqdm(dictionaries):
        #     d.reweight(node_weight)
        # print("Feature scoring")
        # time.sleep(0.1)
        # features = self.feature_select(dictionaries)
        # print("Feature creation")
        # time.sleep(0.1)
        # feature_mat = []
        # for d in tqdm.tqdm(dictionaries):
        #     vec = []
        #     for i in features:
        #         vec.append(VecDict.similarity(d, dictionaries[i], sim_type=self.params["similarity_type"]))
        #     feature_mat.append(vec)
        # return np.array(feature_mat)


SGESParams = {"steps_start": 1,  # The smallest number of steps in a walk
              "steps_stop": 5,  # The biggest number of steps in a walk
              "steps_step": 1,  # Distance between two walk values
              "node_appender": "list",  # Name of the function that appends nodes: binary, rules, list, string, custom
              "walk_appender": "array",  # Name of the function that appends walks: pattern, array, backtrack, custom
              "num_walks": 2000,  # Number of walks per node
              "distribution": "uniform",  # Name of the distribution: moyal, levy, uniform, custom
              "feature_inclusion_threshold": 0.005,  # Threshold for for including a feature
              "similarity_type": "cosine",  # Similarity calculation function: cosine, jaccard
              "num_of_features": 1750}  # Number of features
