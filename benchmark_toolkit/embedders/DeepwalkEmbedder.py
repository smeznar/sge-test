import sys
import numpy as np
from networkx import from_scipy_sparse_matrix

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)
from embedder import Embedder
from deepwalk.deepwalk.__main__ import process


class DeepwalkEmbedder(Embedder):
    def name(self):
        return 'deepwalk'

    def notes(self):
        return "Deepwalk embedder (Bryan Perozzi implementation)"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(DEParams)
        self.shape = graph.shape
        self.graph = from_scipy_sparse_matrix(graph)

    def embed(self):
        model = process(self.graph, self.params)
        return np.array([(model.wv[str(node)] if (str(node) in model.wv) else [0]*self.params["representation_size"]) for node in self.graph.nodes])


DEParams = {"representation_size": 128,  # Number of latent dimensions to learn for each node
            "number_walks": 10,  # Number of random walks to start at each node
            "walk_length": 80,  # Length of the random walk started at each node
            "window_size": 10,  # Window size of skipgram model
            "workers": 8,  # Number of parallel processes
            "max_memory_data_size": 1000000000,  # Size to start dumping walks to disk,
            # instead of keeping them in memory
            "seed": 0,  # Seed for random walk generator
            "vertex_freq_degree": False,  # Use vertex degree to estimate the frequency of nodes
            # in the random walks. This option is faster than calculating the vocabulary.
            "undirected": False}  # Is graph directed
