import sys

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from SCD.SCD import SCD_obj


class PPREmbedder(Embedder):
    def name(self):
        return 'Personalized pagerank'

    def notes(self):
        return "Personalized pagerank embedder"

    def initialize(self, network, labels):
        self.params.update(PPRParams)
        node_names = list(range(network.shape[1]))
        self.network = network
        self.obj = SCD_obj(network, node_names=node_names)

    def embed(self):
        return self.obj.get_sparse_walk_matrix(self.params["number_important"],
                                               self.params["probability_threshold"],
                                               self.params["parallel_steps"])


PPRParams = {"number_important": 1000,
             "probability_threshold": 0.0005,
             "parallel_steps": 6}
