import numpy as np

from embedder import Embedder


class TestEmbedder(Embedder):
    def name(self):
        return 'test'

    def notes(self):
        return "testing embedder"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(TEParams)
        self.rows = graph.shape[0]
        print("code for initialization")

    def embed(self):
        print("code to make an embedding")
        return np.random.rand(self.rows, self.params["num_of_features"])


TEParams = {"num_of_features": 50}
