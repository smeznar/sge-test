import sys
# from Constants import THIS_LIB_SRC_FOLDER
sys.path.append("/Users/smeznar/Projects/SNoRe/src")

from embedder import Embedder
from snore import SNoRe


class SNoReEmbedder(Embedder):
    def name(self):
        return 'SNoRe'

    def notes(self):
        return "fast SNoRe"

    def initialize(self, network, labels):
        # Update the params dictionary with significant parameters
        self.params.update(SParams)
        self.network = network
        self.labels = labels
        self.model = SNoRe(dense_dimension=self.params["num_of_features"], inclusion=self.params["feature_inclusion_threshold"],
                           num_walks=self.params["num_walks"], max_walk_length=self.params["steps_stop"])

    def embed(self):
        return self.model.embed(self.network)


SParams = {"steps_start": 1,  # The smallest number of steps in a walk
           "steps_stop": 5,  # The biggest number of steps in a walk
           "num_walks": 1024,  # Number of walks per node
           "distribution": "uniform",  # Name of the distribution: moyal, levy, uniform, custom
           "feature_inclusion_threshold": 0.005,  # Threshold for for including a feature
           "similarity_type": "jaccard",  # Similarity calculation function: cosine, jaccard
           "num_of_features": 256}  # Number of features
