import sys
import numpy as np
from networkx import from_scipy_sparse_matrix

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from Line.ge import LINE


class LineEmbedder(Embedder):
    def name(self):
        return 'line'

    def notes(self):
        return "line"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(LEParams)
        self.graph = from_scipy_sparse_matrix(graph)

    def embed(self):
        model = LINE(self.graph, embedding_size=self.params["embedding_size"], negative_ratio=self.params["negative_ratio"],
                     order=self.params["order"])
        model.train(self.params["batch_size"], self.params["epochs"], self.params["initial_epoch"],
                    self.params["verbose"], self.params["times"])
        emb = model.get_embeddings()
        return np.array([emb[i] for i in range(len(emb))])


LEParams = {"embedding_size": 200,  # Number of latent dimensions to learn for each node
            "negative_ratio": 5,  # Negative ratio
            "order": 'second',  # One of 'first','second','all'
            "batch_size": 1024,
            "epochs": 50,
            "initial_epoch": 0,
            "verbose": 2,
            "times": 1}
