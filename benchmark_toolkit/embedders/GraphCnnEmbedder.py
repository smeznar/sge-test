import sys

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from powerful_gnns import main


class GraphCnnEmbedder(Embedder):
    def name(self):
        return 'Personalized pagerank'

    def notes(self):
        return "Personalized pagerank embedder"

    def initialize(self, network, labels):
        self.params.update(PPRParams)
        node_names = list(range(network.shape[1]))
        self.network = network
        self.obj = SCD_obj(network, node_names=node_names)

    def embed(self):
        return self.obj.get_sparse_walk_matrix(self.params["number_important"],
                                               self.params["probability_threshold"],
                                               self.params["parallel_steps"])


PPRParams = {'device': 0,
             'batch_size': 32,
             'iters_per_epoch': 50,
             'epochs': 350,
             'lr': 0.01,
             'seed': 0,
             'fold_idx': 0,
             'num_layers': 5,
             'num_mlp_layers': 2,
             'hidden_dim': 64,
             'final_dropout': 0.5,
             'graph_pooling_type': "sum",  #choices=["sum", "average"],
             'neighbor_pooling_type': "sum",  # choices=["sum", "average", "max"],
             'learn_eps': True,
             'degree_as_tag': True}

