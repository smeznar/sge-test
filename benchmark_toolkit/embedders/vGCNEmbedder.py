import sys
from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)


from GAE.src_gae_lib import benchmark_gae
from embedder import Embedder


class vGCNEmbedder(Embedder):
    def name(self):
        return 'vGCN'

    def notes(self):
        return "vGCN embedder"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(vEParams)
        self.graph = graph

    def embed(self):
        roc_score, ap_score, embedding = benchmark_gae(self.graph, 1, self.params)
        print("ROC:", roc_score, "AP:", ap_score)
        return embedding


vEParams = {"learning_rate": 0.01,  # Initial learning rate
            "epochs": 200,  # Number of epochs to train
            "hidden1": 32,  # Number of units in hidden layer 1
            "hidden2": 16,  # Number of units in hidden layer 2
            "weight_decay": 0.,  # Weight for L2 loss on embedding matrix
            "dropout": 0.,  # Dropout rate (1 - keep probability).
            "model": "gcn_ae",  # possible models: gcn_ae, gcn_vae
            "features": 0}  # Whether to use features (1) or not (0)
