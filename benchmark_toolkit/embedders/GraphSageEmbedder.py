import sys
import numpy as np
from networkx import from_scipy_sparse_matrix

from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from GraphSAGE.graphsage import unsupervised_train
from GraphSAGE.graphsage import utils


class GraphSageEmbedder(Embedder):
    def name(self):
        return 'GraphSage'

    def notes(self):
        return "GraphSage"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(GSEParams)
        self.node_len = graph.shape[0]
        self.graph = from_scipy_sparse_matrix(graph)
        # TODO: Split into train, test, validation and create subgraph
        self.walks = utils.run_random_walks(self.graph, [n for n in range(graph.shape[0])])

    def embed(self):
        emb = unsupervised_train.train((self.graph, None, {i: i for i in range(self.node_len)}, self.walks),
                                       self.params, {n: n for n in range(self.node_len)})
        return np.array([emb[0][i, :] for i in emb[1]])


GSEParams = {'log_device_placement': False,  # Whether to log device placement.
             'model': 'graphsage_mean',  # model names. See README for possible values
             'learning_rate': 0.00001,  # initial learning rate.
             "model_size": "small",  # Can be big or small; model specific def'ns
             'train_prefix': '',  # name of the object file that stores the training data. must be specified.
             'epochs': 1,  # number of epochs to train.
             'dropout': 0.0,  # dropout rate (1 - keep probability)
             'weight_decay': 0.0,  # weight for l2 loss on embedding matrix.
             'max_degree': 100,  # maximum node degree.
             'samples_1': 25,  # number of samples in layer 1
             'samples_2': 10,  # number of users samples in layer 2
             'dim_1': 128,  # Size of output dim (final is 2x this, if using concat)
             'dim_2': 128,  # Size of output dim (final is 2x this, if using concat)
             'random_context': True,  # Whether to use random context or direct edges
             'neg_sample_size': 20,  # number of negative samples
             'batch_size': 512,  # minibatch size.
             'n2v_test_epochs': 1,  # Number of new SGD epochs for n2v.
             'identity_dim': 20,  # Set to positive value to use identity embedding features of that dimension. Default 0
             'save_embeddings': True,  # whether to save embeddings for all nodes after training
             'base_log_dir': '.',  # base directory for logging and saving embeddings
             'validate_iter': 5000,  # how often to run a validation minibatch.
             'validate_batch_size': 256,  # how many nodes per validation sample.
             'gpu': 1,  # which gpu to use.
             'print_every': 50,  # How often to print training info.
             'max_total_steps': 10**10}  # Maximum total number of iterations
