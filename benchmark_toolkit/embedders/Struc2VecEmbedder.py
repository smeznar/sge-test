import sys
import numpy as np
from networkx import from_scipy_sparse_matrix
from Constants import EXTERNAL_LIB_FOLDER
sys.path.append(EXTERNAL_LIB_FOLDER)

from embedder import Embedder
from struc2vec.src.main import exec_struc2vec, learn_embeddings
from struc2vec.src.graph import from_networkx


# Before testing this embedder change the temp folder path in the utils.py and main.py scripts in the struc2vec library

class Struc2VecEmbedder(Embedder):
    def name(self):
        return 'node2vec'

    def notes(self):
        return "embedder for the node2vec algorithem (Aditya Grover implementation)"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(S2VEParams)
        self.graph = from_scipy_sparse_matrix(graph)
        self.G = from_networkx(self.graph, not self.params["is_directed"])

    def embed(self):
        exec_struc2vec(self.G, self.params)
        model = learn_embeddings(self.params)
        return np.array([model.wv[str(node)] for node in self.graph.nodes])


S2VEParams = {"dimensions": 128,  # Number of dimensions (features)
            "walk_length": 80,  # Length of the walk per source
            "num_walks": 10,  # Number of walks per source
            "window_size": 10,  # Context size for optimization
            "iter": 5,  # Number of epochs in SGD
            "workers": 4,  # Number of workers
            "until-layer": None,  # Calculate until layer n (None for all)
            "OPT1": True,  # Compact bfs pre-process of neighbours optimization
            "OPT2": True,  # Optimization 2
            "OPT3": False,  # Until layer optimization
            "is_weighted": False,  # Is graph weighted
            "is_directed": False}  # Is graph directed
