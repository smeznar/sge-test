import numpy as np
import subprocess
import sys
import time
import tqdm
from sklearn import preprocessing
from Constants import THIS_LIB_SRC_FOLDER
sys.path.append(THIS_LIB_SRC_FOLDER)

from embedder import Embedder
from SGE import RandomWalkGenerator


temp_dir = "/Users/smeznar/IJS/sge-test/benchmark_toolkit/temp/"
orca_dir = "/Users/smeznar/IJS/out_libraries/orca_graphlets/"


class GraphletEmbedder(Embedder):
    def name(self):
        return 'graphlet'

    def notes(self):
        return "testing embedder"

    def initialize(self, graph, labels):
        # Update the params dictionary with significant parameters
        self.params.update(GEParams)
        self.graph = graph
        self.labels = labels
        self.feature_select = lambda d: np.random.permutation(range(d.shape[0]))[:self.params["num_of_features"]]
        graphlets = self.create_graphlets(graph)
        graphlets = graphlets[:, np.where(np.max(graphlets, axis=0) != 0)[0]]
        self.graphlets = graphlets / np.max(graphlets, axis=0)

        walk_generator = RandomWalkGenerator(self.graph, self.labels, self.params)
        self.walks = walk_generator.sample()

    def embed(self):
        print("Walk vectors creation")
        time.sleep(0.1)
        vectors = np.zeros(self.graphlets.shape)
        for i, wa in tqdm.tqdm(enumerate(self.walks)):
            for j, step in wa:
                vectors[i, :] += self.graphlets[j, :]
        vectors = vectors / np.max(vectors, axis=0)
        norms = np.linalg.norm(vectors, axis=1)
        print("Similarity calculation")
        time.sleep(0.1)
        features = self.feature_select(vectors)
        feature_mat = []
        for i in tqdm.tqdm(range(vectors.shape[0])):
            vec = []
            for j in features:
                vec.append(np.dot(vectors[i], vectors[j])/(norms[i]*norms[j]))
            feature_mat.append(vec)
        return preprocessing.scale(np.array(feature_mat))

    def create_graphlets(self, graph):
        indices = np.triu(graph.toarray(), 1).nonzero()
        with open(temp_dir+"orca_edges.in", "w") as file:
            file.write(str(graph.shape[0]) + " " + str(indices[0].shape[0]) + "\n")
            for i, j in zip(*indices):
                file.write(str(i) + " " + str(j) + "\n")
        subprocess.run([orca_dir+"/orca", str(self.params["graphlet_size"]),
                        temp_dir+"orca_edges.in", temp_dir+"orca_edges.out"])
        graphlets = []
        with open(temp_dir+"orca_edges.out", "r") as file:
            for l in file:
                graphlets.append(([1] + [int(count) for count in l.strip().split(" ")]))
        return np.array(graphlets)


GEParams = {"steps_start": 1,  # The smallest number of steps in a walk
            "steps_stop": 5,  # The biggest number of steps in a walk
            "steps_step": 1,  # Distance between two walk values
            "node_appender": "list",  # Name of the function that appends nodes: binary, rules, list, string, custom
            "walk_appender": "array",  # Name of the function that appends walks: pattern, array, backtrack, custom
            "num_walks": 1000,  # Number of walks per node
            "distribution": "uniform",  # Name of the distribution: moyal, levy, uniform, custom
            "graphlet_size": 5,
            "num_of_features": 1000}
