import os, sys
import yaml
import importlib.util
import scipy.io
import numpy as np
from scipy import sparse
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.multiclass import OneVsRestClassifier

from Constants import EMBEDDERS_FOLDER


def read_graph(dataset):
    try:
        mat = scipy.io.loadmat(os.path.join(os.path.dirname(sys.argv[0]), "datasets", dataset + ".mat"))
    except Exception:
        print("Dataset ", dataset, " not found. Make sure that it is inside the datasets folder"
              " or that you spelled the name correctly")
        return None
    network = sparse.csr_matrix(mat['network'])  ## omrezje
    labels = mat['group']  ## labele
    labels_matrix = sparse.csr_matrix(labels)
    labels_count = labels_matrix.shape[1]
    mlb = MultiLabelBinarizer(range(labels_count))
    return network, labels_matrix, mlb


def load_embedder(embedder_id):
    embedder_config = None
    with open(os.path.join(os.path.dirname(sys.argv[0]), 'embedders.yaml'), 'r') as yfile:
        embedders = yaml.load(yfile, Loader=yaml.BaseLoader)
        if embedder_id in embedders:
            embedder_config = embedders[embedder_id]
        else:
            print('Embedder %s is not in the embedders.yaml file.' % embedder_id)
            exit(-1)

    embedder_path = EMBEDDERS_FOLDER + embedder_config['embedder_path']
    embedder_class_name = embedder_config['class_name']

    if not os.path.isfile(embedder_path):
        print('The variable tracker_path is not a file.')
        exit(-1)

    # det directory od the tracker file and add it to PYTHONPATH
    sys.path.insert(0, os.path.dirname(embedder_path))

    # check if another paths are specified and add them

    spec = importlib.util.spec_from_file_location(embedder_class_name, embedder_path)
    module_ = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module_)
    embedder_class = getattr(module_, embedder_class_name)
    return embedder_class


class TopKRanker(OneVsRestClassifier):
    def predict(self, X, top_k_list):
        assert X.shape[0] == len(top_k_list)
        probs = np.asarray(super(TopKRanker, self).predict_proba(X))
        all_labels = []
        for i, k in enumerate(top_k_list):
            probs_ = probs[i, :]
            labels = self.classes_[probs_.argsort()[-k:]].tolist()
            all_labels.append(labels)
        return all_labels