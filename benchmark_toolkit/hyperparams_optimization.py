import argparse
from utils import load_embedder
from bayes_opt import BayesianOptimization


args = None


def evaluate_embedder(inclusion_threshold, num_walks):
    global args
    embedder_class = load_embedder(args.embedder)
    embedder = embedder_class()
    embedder.optimization_params = {"feature_inclusion_threshold": inclusion_threshold, "num_walks": num_walks}
    return embedder.evaluate(args, param_opt=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Graph Embedding Evaluation Utility')
    parser.add_argument('--embedder', help='Embedder identifier', required=True, action='store')
    parser.add_argument('--datasets', help='Names of datasets to be tested. (Bitcoin, Bitcoin_alpha, Blogspot,'
                                           ' citeseer, cora, Homo_sapiens, ions, POS)',
                        required=True, action='store', nargs='*')
    parser.add_argument('--save', help='Save embedding to a file', default=False, action='store_true')
    parser.add_argument("--num-shuffles", default=10, type=int, help='Number of shuffles.')
    parser.add_argument("--all", default=False, action='store_true',
                        help='The embeddings are evaluated on all training percents from 10 to 90 when this flag is set to true. '
                             'By default, only training percent of 50 is used.')
    parser.add_argument("--data_perc", default=0.5, type=float, help='data percentage if all is not chosen.')
    args = parser.parse_args()

    # Bayesian Optimization
    pbounds = {"inclusion_threshold": (0.0005, 0.01), "num_walks": (100, 10000)}
    optimizer = BayesianOptimization(
        f=evaluate_embedder,
        pbounds=pbounds,
        random_state=1
    )
    optimizer.maximize(init_points=1, n_iter=1)
    print(optimizer.max)
    for i, res in enumerate(optimizer.res):
        print("Iteration {}: \n\t{}".format(i, res))
