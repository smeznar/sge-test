import numpy as np
import time
import math
import tqdm
import sys
import networkx as nx
from collections import Counter
from itertools import chain
from scipy import sparse
import community

from SGE import RandomWalkGenerator


class SGESimilarity:
    def __init__(self, num_of_features, feat_thresh=0.005, num_walks=1000, steps_start=1, steps_stop=5,
                 steps_step=1, node_appender="list", walk_appender="array", distribution="uniform", num_communities=100):
        self.num_of_features = num_of_features
        self.walk_params = {"num_walks": num_walks, "steps_start": steps_start, "steps_stop": steps_stop,
                            "steps_step": steps_step, "node_appender": node_appender, "walk_appender": walk_appender,
                            "distribution": distribution}
        # self.num_communities = num_communities
        self.step_weight = lambda s: 1  # 1/(s+1)  # weight function for a step inside a walk
        # input: step od the walk, output: step score
        self.node_feature_weight = lambda s: s  # weight function for feature inside a node
        # input: feature score between 0 and 1, output: re-weighted feature score
        # self.feature_representation_weight = feature_represtentation_all_same
        self.feature_inclusion_threshold = feat_thresh

    def fit(self, network):
        walk_generator = RandomWalkGenerator(network, [], self.walk_params)
        nx_network = nx.from_scipy_sparse_matrix(network)
        # self.communities = community.best_partition(nx_network)
        # com_count = Counter(self.communities.values()).most_common(self.num_communities)
        if nx_network.is_directed():
            con_comp = list(nx.weakly_connected_components(nx_network))
        else:
            con_comp = list(nx.connected_components(nx_network))
        # self.community_features = [t[0] for t in com_count]
        enumerated_nodes = []
        for con in con_comp:
            enumerated_nodes += [int(c) for c in nx.algorithms.traversal.dfs_tree(nx_network, list(con)[0])]
        distance = len(enumerated_nodes)/self.num_of_features
        indexes = [round(distance*i) for i in range(self.num_of_features)]
        self.feature_select = [enumerated_nodes[i] for i in indexes]

        walks = walk_generator.sample(nodes=self.feature_select)
        self.feature_dics = []
        print("creating features")
        for j, wa in enumerate(walks):
            self.feature_dics.append(VecDict(wa, self.step_weight, self.node_feature_weight,
                                             self.feature_inclusion_threshold))
            # if self.communities[j] in self.community_features:
            #     self.feature_dics[j].d["c"+str(self.community_features.index(self.communities[j]))] = 1
            #     self.feature_dics[j].size += 1

        pass
        # feature_freq = Counter(chain.from_iterable([d.document_terms() for d in feature_dics]))

    def transform(self, network, similarity_type="cosine"):
        walk_generator = RandomWalkGenerator(network, [], self.walk_params)
        walks = walk_generator.sample()
        print("Walk scoring")
        time.sleep(0.1)
        dictionaries = []
        for j, wa in enumerate(tqdm.tqdm(walks)):
            dictionaries.append(VecDict(wa, self.step_weight, self.node_feature_weight,
                                        self.feature_inclusion_threshold))
            # if self.communities[j] in self.community_features:
            #     dictionaries[j].d["c"+str(self.community_features.index(self.communities[j]))] = 1
            #     dictionaries[j].size += 1
        #print("Reweight")
        #time.sleep(0.1)
        #node_freq = Counter(chain.from_iterable([d.document_terms() for d in dictionaries]))
        #node_weight = self.feature_representation_weight(node_freq, len(self.walks))
        #for d in tqdm.tqdm(dictionaries):
        #    d.reweight(node_freq)
        print("Feature creation")
        time.sleep(0.1)
        feature_mat = []
        for d in tqdm.tqdm(dictionaries):
            vec = []
            for fd in self.feature_dics:
                vec.append(VecDict.similarity(d, fd, sim_type=similarity_type))
            feature_mat.append(vec)
        return sparse.csr_matrix(np.array(feature_mat))


def feature_represtentation_all_same(document_freq: Counter, doc_num: int):
    return {c: 1 for c in list(document_freq)}


class VecDict:
    def __init__(self, arr, step_weight, frequency_weight, inclusion_threshold):
        self.d = {}
        self.size = 0
        d = {}
        for node, step in arr:
            if node not in d:
                d[node] = step_weight(step)
            else:
                d[node] += step_weight(step)
        m = sum(d.values())
        for key, value in d.items():
            weight = frequency_weight(value/m)
            if weight >= inclusion_threshold:
                self.d[key] = weight

        s = sum(d.values())
        for key, value in d.items():
            self.d[key] = value/s
            self.size += self.d[key] ** 2
        self.size = math.sqrt(self.size)

    def document_terms(self):
        return list(self.d.keys())

    def reweight(self, scores):
        for k, v in self.d.items():
            self.d[k] = v*scores[k]

    @staticmethod
    def similarity(d1, d2, sim_type="cosine"):
        if sim_type == "jaccard":
            return VecDict.jaccard_sim(d1, d2)
        return VecDict.cosine_sim(d1, d2)

    @staticmethod
    def jaccard_sim(d1, d2):
        intersection = len(d1.d.keys() & d2.d.keys())
        return intersection/(len(d1.d.keys()) + len(d2.d.keys()) - intersection)

    @staticmethod
    def cosine_sim(d1, d2):
        keys = d1.d.keys() & d2.d.keys()
        score = 0
        for k in keys:
            score += d1.d[k] * d2.d[k]
        return score / (d1.size * d2.size)


SGESParams = {"steps_start": 1,  # The smallest number of steps in a walk
              "steps_stop": 5,  # The biggest number of steps in a walk
              "steps_step": 1,  # Distance between two walk values
              "node_appender": "list",  # Name of the function that appends nodes: binary, rules, list, string, custom
              "walk_appender": "array",  # Name of the function that appends walks: pattern, array, backtrack, custom
              "num_walks": 2000,  # Number of walks per node
              "distribution": "uniform",  # Name of the distribution: moyal, levy, uniform, custom
              "feature_inclusion_threshold": 0.005,  # Threshold for for including a feature
              "similarity_type": "cosine",  # Similarity calculation function: cosine, jaccard
              "num_of_features": 2500}  # Number of features
