from numba import jit, prange
import numpy as np
from scipy.stats import moyal
import scipy.io
import tqdm
import networkx
import itertools
import scipy.sparse
import math
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import chi2, SelectKBest
from collections import Counter
from itertools import chain
import time


def load_matrix(path):
    mat = scipy.io.loadmat(path)

    network = mat['network']  ## omrezje
    labels = mat['group']  ## labele
    return network, labels


@jit(parallel=True, nogil=True, nopython=True)
def numba_walk_kernel(walk_matrix, node_name, sparse_pointers, sparse_neighbors, num_steps=3, num_walks=100):
    for walk in prange(num_walks):
        curr = node_name
        offset = walk * (num_steps + 1)
        walk_matrix[offset] = node_name
        for step in prange(num_steps):
            num_neighs = sparse_pointers[curr+1] - sparse_pointers[curr]
            if num_neighs > 0:
                curr = sparse_neighbors[sparse_pointers[curr] + np.random.randint(num_neighs)]
            idx = offset+step+1
            walk_matrix[idx] = curr


class RandomWalkGenerator:
    def __init__(self, network, labels, params, dist=None, steps=None):
        #  params: node_appender, distribution, steps, steps_start, steps_stop, steps_step,
        #  walk_appender, walks_num
        self.params = params
        self.network = network.tocsr()
        self.labels = labels
        self.sparse_pointers = self.network.indptr
        self.sparse_neighbors = self.network.indices
        self.target_node_map = {}
        self.target_nodes = []
        self.label_vector = []
        self.net = networkx.from_scipy_sparse_matrix(self.network)
        self.steps = steps

        if self.params["distribution"] == "custom":
            self.dist = dist
        else:
            self.dist = self.generate_dist()

        for enx, node in enumerate(self.net.nodes(data=True)):
            self.target_node_map[node[0]] = enx
            self.target_nodes.append(node[0])
        self.final_documents = []

    def generate_dist(self):
        walk_len = ((self.params["steps_stop"]+1) - self.params["steps_start"]) // self.params["steps_step"]
        if self.params["distribution"] == "moyal":
            r = moyal.rvs(size=self.params["num_walks"])
            inds = np.histogram(r, walk_len)
        elif self.params["distribution"] == "levy":
            r = moyal.rvs(size=self.params["num_walks"])
            inds = np.histogram(r, walk_len)
        elif self.params["distribution"] == "uniform":
            r = np.random.uniform(0, 1, self.params["num_walks"])
            inds = np.histogram(r, walk_len)
        else:
            raise Exception("Distribution not supported, use custom")
        return inds[0]

    def sample(self, node_appender=None, walk_appender=None, nodes=None):
        if nodes is None:
            nodes = self.target_nodes
        for node in tqdm.tqdm(nodes):
            #self.label_vector.append(int(node[1]))
            node_words = self.sample_neighborhood(self.target_node_map[node], walk_appender)
            node_words = itertools.chain(*node_words)

            if self.params["node_appender"] == "binary":
                self.final_documents.append(" ".join(set(node_words)))  ## counts do not matter here.

            elif self.params["node_appender"] == "rules":
                self.final_documents.append(set(node_words))
            elif self.params["node_appender"] == "list":
                self.final_documents.append(list(node_words))
            elif self.params["node_appender"] == "string":
                self.final_documents.append(" ".join(node_words))
            else:
                self.final_documents.append(node_appender(node_words))
        return self.final_documents

    def sample_neighborhood(self, node_name, walk_appender=None):
        """
        Very fast node sampling.
        """
        walk_struct = []
        if self.steps is not None:
            steps = self.steps
        else:
            steps = range(self.params["steps_start"], self.params["steps_stop"]+1, self.params["steps_step"])

        for enx, i in enumerate(steps):
            wlen = self.dist[enx]
            walk_matrix = -np.ones((wlen, (i+1)), dtype=np.int32, order='C')
            walk_matrix = np.reshape(walk_matrix, (walk_matrix.size,), order='C')
            numba_walk_kernel(walk_matrix, node_name, self.sparse_pointers, self.sparse_neighbors, num_steps=i, num_walks=wlen)
            if self.params["walk_appender"] == "pattern":
                walk_string = [str(x)+"_"+str(i) for x in walk_matrix]
            elif self.params["walk_appender"] == "array":
                walk_string = [(x, (j % (i+1))) for j, x in enumerate(walk_matrix)]
            elif self.params["walk_appender"] == "backtrack":
                walk_string = []
                one_walk = []
                for j in range(len(walk_matrix)):
                    one_walk.append(walk_matrix[j])
                    if (j % (i+1)) == i:
                        for k in range(1, i+2):
                            walk_string.append("_".join([str(n) for n in one_walk[-k:]]))
                        one_walk = []
            else:
                walk_string = walk_appender(walk_matrix, i)
            walk_struct.append(walk_string)
        return walk_struct


class ImportanceVec:
    def __init__(self, weight_fun):
        self.weight_fun = weight_fun

    def fit(self, data, labels):
        lm = scipy.sparse.lil_matrix((len(data), len(data)), dtype=np.float32)
        for i in tqdm.tqdm(range(len(data))):
            node = data[i]
            for step in node:
                lm[i, step[0]] += self.weight_fun(step[1])
        return lm


if __name__ == '__main__':
    #net, lab = load_matrix("../benchmark_toolkit/datasets/citeseer.mat")
    #walk_params = WalkParams()
    #a = NodeVectorizer(net, lab, walk_params)
    #a.sample("list")
    #b = a.vectorize(FrequencyVec(lambda a: 1/(a+1), lambda x: np.log2(1+x), tf_idf_doc_weigh, 2500))
    #np.savetxt("../embeddings/cite_test3.csv", b)
    pass
