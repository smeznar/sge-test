#!/usr/bin/env python

import numpy
import sys
import json
import os

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from collections import defaultdict
from gensim.models import Word2Vec, KeyedVectors
from six import iteritems
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from scipy.io import loadmat
# from sklearn.metrics import accuracy_score
from sklearn.utils import shuffle as skshuffle
from sklearn.preprocessing import MultiLabelBinarizer
# import multiprocessing as mp
from scipy import sparse


class EmbeddingTester:
    def __init__(self, embedding, labels, mlb):
        self.X = embedding
        self.y = labels
        self.mlb = mlb
        self.results = defaultdict(list)

    def test(self, num_shuffles=10, all_percentages=False, data_perc=0.5):
        shuffles = []
        for x in range(num_shuffles):
            shuffles.append(skshuffle(self.X, self.y))

        if all_percentages:
            training_percents = numpy.asarray(range(1, 10)) * .1
        else:
            training_percents = [data_perc]
        for train_percent in training_percents:
            for shuf in shuffles:
                X, y = shuf

                training_size = int(train_percent * X.shape[0])

                X_train = X[:training_size, :]
                y_train_ = y[:training_size]
                print(X_train.shape, y_train_.shape)
                y_train = [[] for x in range(y_train_.shape[0])]

                cy = y_train_.tocoo()
                for i, j in zip(cy.row, cy.col):
                    y_train[i].append(j)

                assert sum(len(l) for l in y_train) == y_train_.nnz

                X_test = X[training_size:, :]
                y_test_ = y[training_size:]

                y_test = [[] for _ in range(y_test_.shape[0])]

                cy = y_test_.tocoo()
                for i, j in zip(cy.row, cy.col):
                    y_test[i].append(j)

                clf = TopKRanker(LogisticRegression())
                clf.fit(X_train, y_train_)

                # find out how many labels should be predicted
                top_k_list = [len(l) for l in y_test]
                preds = clf.predict(X_test, top_k_list)

                results = {}
                averages = ["micro", "macro"]
                for average in averages:
                    results[average] = f1_score(self.mlb.fit_transform(y_test),
                                                self.mlb.fit_transform(preds), average=average)

                self.results[train_percent].append(results)

    def write_results(self, filepath, dataset_name, description):
        if os.path.exists(filepath):
            with open(filepath, "r") as file:
                json_file = json.load(file)
        else:
            json_file = {"name": dataset_name, "results": []}
        json_file["results"].append({"desc": description, "score": self.results})
        with open(filepath, 'w') as file:
            json.dump(json_file, file)


def main():
    parser = ArgumentParser("scoring",
                            formatter_class=ArgumentDefaultsHelpFormatter,
                            conflict_handler='resolve')
    parser.add_argument("--emb", help='Embeddings file')
    parser.add_argument("--network", required=True,
                        help='A .mat file containing the adjacency matrix and node labels of the input network.')
    parser.add_argument("--adj-matrix-name", default='network',
                        help='Variable name of the adjacency matrix inside the .mat file.')
    parser.add_argument("--label-matrix-name", default='group',
                        help='Variable name of the labels matrix inside the .mat file.')
    parser.add_argument("--num-shuffles", default=10, type=int, help='Number of shuffles.')
    parser.add_argument("--all", default=False, action='store_true',
                        help='The embeddings are evaluated on all training percents from 10 to 90 when this flag is set to true. '
                             'By default, only training percents of 10, 50 and 90 are used.')
    parser.add_argument("--filetype", default="csv",
                        help='')

    args = parser.parse_args()
    # 0. Files
    embeddings_file = args.emb
    matfile = args.network

    # 1. Load labels
    mat = loadmat(matfile)
    A = mat[args.adj_matrix_name]
    nodelen = A.shape[0]
    labels_matrix = sparse.csr_matrix(mat[args.label_matrix_name])
    labels_count = labels_matrix.shape[1]
    mlb = MultiLabelBinarizer(range(labels_count))

    # 2. Load Embeddings
    # and map nodes to their features (note:  assumes nodes are labeled as integers 1:N)
    if args.filetype == "csv":
        fm1 = numpy.genfromtxt("/Users/smeznar/IJS/sge-test/benchmark_toolkit/embeddings/graphlet_cora_emb.csv", skip_header=False)
        fm2 = numpy.genfromtxt("/Users/smeznar/IJS/sge-test/benchmark_toolkit/embeddings/SGESimilarity_cora_emb.csv", skip_header=False)
        #features_matrix = numpy.genfromtxt(embeddings_file, skip_header=False)
        features_matrix = numpy.hstack((fm2, fm1))
    else:
        model = KeyedVectors.load_word2vec_format(embeddings_file, binary=False)
        features_matrix = numpy.asarray([model[str(node)] for node in range(nodelen)])

    emb_test = EmbeddingTester(features_matrix, labels_matrix, mlb)
    emb_test.test(args.num_shuffles, args.all)
    #emb_test.write_results("/Users/smeznar/IJS/sge-test/results/sge_similarity_neki.json", "citeseer", "nevem")
    all_results = emb_test.results

    print('Results, using embeddings of dimensionality', features_matrix.shape[1])
    print('-------------------')
    for train_percent in sorted(all_results.keys()):
      print('Train percent:', train_percent)
      for index, result in enumerate(all_results[train_percent]):
        print('Shuffle #%d:   ' % (index + 1), result)
      avg_score = defaultdict(float)
      for score_dict in all_results[train_percent]:
        for metric, score in iteritems(score_dict):
          avg_score[metric] += score
      for metric in avg_score:
        avg_score[metric] /= len(all_results[train_percent])
      print('Average score:', dict(avg_score))
      print('-------------------')


class TopKRanker(OneVsRestClassifier):
    def predict(self, X, top_k_list):
        assert X.shape[0] == len(top_k_list)
        probs = numpy.asarray(super(TopKRanker, self).predict_proba(X))
        all_labels = []
        for i, k in enumerate(top_k_list):
            probs_ = probs[i, :]
            labels = self.classes_[probs_.argsort()[-k:]].tolist()
            all_labels.append(labels)
        return all_labels


if __name__ == "__main__":
  sys.exit(main())


