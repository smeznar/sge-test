import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


sns.set_style("whitegrid")
from plotnine import *

    
## column names
#sns.set_style("white")
palette="Set3"
#cnames = ["percent_train","classifier","setting","micro_F","micro_F_sd","macro_F","macro_F_sd","dataset","time"]
cnames = ["percent_train","micro_F","macro_F","setting","dataset","time"]

## dependent on the coverage
## dependent on the time - execution
## variability
## different settings between eachother

def plot_core_macro(fname):

    fnamex = pd.read_csv(fname,sep=" ")
    print(fnamex)
    print(fnamex.head())
    
    p1 = sns.boxplot('percent_train','macro_F',hue='setting', data=fnamex)
    plt.show()
    return 1

def plot_core_micro(fname):

    fnamex = pd.read_csv(fname,sep=" ")    
    grid = sns.FacetGrid(fnamex, col="dataset", hue="setting", col_wrap=2)
    grid.map(plt.plot, "percent_train", "micro_F",marker="o").add_legend()
    plt.show()

    
def plot_core_macro_gg(fnamex):
    print(fnamex.columns)
    fx = fnamex.groupby(["setting"])['macro_F'].mean().sort_values().index.values
    g = sns.FacetGrid(fnamex, col="dataset",  hue="setting",col_wrap=3)    
    g = (g.map(plt.plot, "percent_train","macro_F",marker="o",linewidth=1).add_legend())
    g.set_xlabels("Train percentage (%)")
    g.set_ylabels("Average Macro F1")
    plt.show()

    ax = sns.boxplot(x="setting", y="macro_F",data=fnamex,color="white",order=fx)
    plt.xticks(rotation=45)
    plt.xlabel("Algorithm")
    plt.ylabel("Average Macro F1")
    plt.show()
    
    # gx = (ggplot(fnamex, aes('percent_train', 'macro_F',shape='setting', color='setting'))
    #  + geom_point() + geom_smooth(size=0.5)  + facet_wrap('~dataset') + theme_classic()
    #       +xlab("Percentage of data used for training (%)")
    #       +ylab("Macro F1 score")
    #       + theme(axis_text=element_text(size=12))
    #       + theme(axis_title=element_text(size=15))
    # )
    # gx.draw()
    # plt.show()


def plot_core_micro_gg(fnamex):

    fx = fnamex.groupby(["setting"])['micro_F'].mean().sort_values().index.values
    print(fnamex.columns)
    g = sns.FacetGrid(fnamex, col="dataset",  hue="setting",col_wrap=3)
    g = (g.map(plt.plot, "percent_train","micro_F",marker="o",linewidth=1).add_legend())
    g.set_xlabels("Train percentage (%)")
    g.set_ylabels("Average Micro F1")
    plt.show()

    ax = sns.boxplot(x="setting", y="micro_F",data=fnamex,color="white",order=fx)
    plt.xticks(rotation=45)
    plt.xlabel("Algorithm")
    plt.ylabel("Average Micro F1")
    plt.show()
    
    # gx = (ggplot(fnamex, aes('percent_train', 'micro_F',color='setting',shape='setting'))
    #       + geom_point(size=2) + geom_smooth(size=0.5)  + facet_wrap('~dataset') + theme_classic()
    #       + xlab("Percentage of data used for training (%)")
    #       + ylab("Micro F1 score")
    #       + theme(axis_text=element_text(size=12),
    #               axis_title=element_text(size=15))
    # )
    # gx.draw()
    # plt.show()


def plot_core_time_gg(fname):

    fnamex = pd.read_csv(fname,sep=" ")

    gx = (ggplot(fnamex, aes('setting', 'time'))
          + geom_boxplot()
          + theme(axis_text_x=element_text(rotation=90, hjust=1))
          + theme_bw()
          + facet_wrap('~dataset')
          )
    gx.draw()
    plt.show()

def plot_core_variability(fname):

    fname = pd.read_csv(fname,separator=" ")
    ## for each dataset, take all variability and get it to a box plot

def plot_core_time(fnamex):

    print(fnamex.head())
    px = sns.boxplot("setting","time",data=fnamex)
    plt.show()
    return 1

def plot_mean_times(fn):

    ## for each dataset, plot times.
    fx = fnamex.groupby(["setting"])['time'].mean().sort_values().index.values
    rkx = fn.groupby(['dataset', 'setting'])['time'].mean()
    dx2 = rkx.reset_index()
    # gx = (ggplot(dx2, aes('setting', 'time',shape='dataset', color='dataset'))
    #  + geom_point(size=5) + theme_classic()
    #       +xlab("Algorithm")
    #       +ylab("Average time spent (s)")
    #       + theme(axis_text_x=element_text(rotation=90, hjust=1))
    #       + theme(axis_text=element_text(size=30))
    #       + theme(axis_title=element_text(size=30))
    # )
    # gx.draw()
    # plt.show()


    ax = sns.boxplot(x="setting", y="time",data=fnamex,order=fx,palette="summer")
    plt.xticks(rotation=45)
    ax.set_xlabel("Algorithm",fontsize=20)
    ax.set_ylabel("Average execution time (s)",fontsize=20)
    ax.set_yscale('log')
    ax.tick_params(labelsize=15)
    plt.show()

def plot_robustness(infile):

#    infile = infile[infile['percent_train'] == 0.5].reset_index()
    # plot = sns.distplot(infile['macro_F'])
    # plot.set_xlabel("Macro F score",fontsize=20)
    # plot.set_ylabel("Number of runs",fontsize=20)
    # plt.show()

    print(infile.head())
#    infile['percent_train'] = pd.to_numeric(infile['percent_train'])
    infile = infile[infile['percent_train'] < 0.6]
    p1 = sns.boxplot('percent_train','macro_F', data=infile)
    p1.set_xlabel("Train percent",fontsize=20)
    p1.set_ylabel("Macro F score",fontsize=20)
    plt.show()


    p1 = sns.boxplot('percent_train','micro_F', data=infile)
    p1.set_xlabel("Train percent",fontsize=20)
    p1.set_ylabel("Micro F score",fontsize=20)
    plt.show()

    gx = (ggplot(infile, aes('macro_F','micro_F'))
          + geom_density_2d(aes(color='percent_train'))
          +xlab("Macro F score")
          +ylab("Micro F score")
          + theme(axis_text=element_text(size=15))
          + theme(axis_title=element_text(size=15))
          + theme_bw()
    )
    gx.draw()
    plt.show()


if __name__ == "__main__":

    ## klic funkcij
