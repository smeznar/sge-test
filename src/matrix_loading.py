import scipy.io as sp
import scipy.sparse as ssp


if __name__ == '__main__':
    matrix_name = "../data/citeseer.mat"
    mat = sp.loadmat(matrix_name)
    core_network = mat['network']  ## omrezje
    labels = ssp.lil_matrix(mat['group'].shape, dtype=int)  ## labele
    pass