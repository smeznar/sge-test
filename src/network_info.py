import networkx as nx
import scipy.io
import scipy.sparse as sparse
import numpy as np
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    mat_file = "/Users/smeznar/IJS/node_importance/data/soc-hamsterster.edges.mat"

    mat = scipy.io.loadmat(mat_file)
    network = sparse.csr_matrix(mat['graph'])
    # labels = np.where(mat["group"]>0)[1]
    nx_net = nx.from_scipy_sparse_matrix(network)
    # pos = nx.spring_layout(nx_net)
    # nx.draw_networkx_nodes(nx_net, pos=pos, node_size=2, node_color=labels)
    # nx.draw_networkx_edges(nx_net, pos=pos, edgelist=nx_net.edges, edge_color='k', alpha=0.2)
    # plt.axis('off')
    # plt.gca().set_position([0, 0, 1, 1])
    # plt.savefig("pubmed", quality=95, dpi=600)
    # print(len(labels[np.sum(labels, axis=1) > 1]))
    print("The network has:")
    print(nx_net.is_directed())
    print(str(len(nx_net.nodes))+" nodes")
    print(str(len(nx_net.edges))+" edges")
    print(str(len(list(nx.connected_components(nx_net))))+" connected components")
    print(str((len(nx_net.edges)/len(nx_net.nodes))*2) + " mean edges per node")
    print(str(len(nx_net.edges)/((len(nx_net.nodes)*(len(nx_net.nodes)-1))/2)) + " density")
    # print(str(labels.shape[1])+" classes")
