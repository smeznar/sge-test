import time
import networkx as nx
import numpy as np
from scipy import sparse
import sklearn.metrics.pairwise as pw
from sklearn.preprocessing import normalize
from numba import jit
from SGE import RandomWalkGenerator


@jit(nopython=True)
def nonzero(y, x, ri, p):
    points = set(p)
    rows = 0
    for i in ri:
        si = i
        last = y[i]
        while si < len(y) and y[si] == last:
            if x[i] in points:
                rows += 1
                break
            si += 1
    return rows


@jit(nopython=True)
def create_row_indices(y):
    last = -1
    indices = []
    for i in range(len(y)):
        if y[i] > last:
            indices.append(i)
            last = y[i]
    return indices


class SNoRe:
    def __init__(self, dense_num_features, feat_thresh=0.005, num_walks=1000, steps_start=1, steps_stop=5,
                 distribution="uniform"):
        self.dense_num_features = dense_num_features
        self.walk_params = {"num_walks": num_walks, "steps_start": steps_start, "steps_stop": steps_stop,
                            "steps_step": 1, "node_appender": "list", "walk_appender": "array",
                            "distribution": distribution}
        self.feature_inclusion_threshold = feat_thresh
        self.walk_hash_fn = lambda a: 1

    def fit(self, network):
        walk_generator = RandomWalkGenerator(network, [], self.walk_params)
        nx_network = nx.from_scipy_sparse_matrix(network)
        pagerank = nx.pagerank_scipy(nx_network)
        pr = np.argsort([pagerank[i] for i in range(len(pagerank))])[::-1]
        # a = nx.hits_scipy(nx_network)
        # self.indexes = np.argsort([a[0][i] + a[1][i] for i in range(len(a[0]))])[::-1][:min(self.num_of_features, len(a[0]))]
        # # self.communities = community.best_partition(nx_network)
        # # com_count = Counter(self.communities.values()).most_common(self.num_communities)
        # if nx_network.is_directed():
        #     con_comp = list(nx.weakly_connected_components(nx_network))
        # else:
        #     con_comp = list(nx.connected_components(nx_network))
        # # self.community_features = [t[0] for t in com_count]
        # enumerated_nodes = []
        # for con in con_comp:
        #     enumerated_nodes += [int(c) for c in nx.algorithms.traversal.dfs_tree(nx_network, list(con)[0])]
        # distance = len(enumerated_nodes)/self.num_of_features
        # self.indexes = [round(distance*i) for i in range(self.num_of_features)]
        # self.feature_select = [enumerated_nodes[i] for i in self.indexes]

        # walks = walk_generator.sample(nodes=self.indexes)
        stime = time.time()
        walks = walk_generator.sample()
        print("Feature selection")
        self.mat = self.walk_to_matrix(walks)
        print(time.time()-stime)
        self.indexes = self.generate_indexes(self.mat, pr)

    def transform(self, network, similarity_type):
        # walk_generator = RandomWalkGenerator(network, [], self.walk_params)
        # walks = walk_generator.sample()
        # print("Walk scoring")
        # neigh_mat = self.walk_to_matrix(walks, network.shape)
        # print("Feature creation")
        # time.sleep(0.1)
        # return pw.pairwise_distances(neigh_mat, self.feature_mat, metric='euclidean')
        print("Feature creation")
        a = pw.cosine_similarity(self.mat, self.mat[self.indexes], dense_output=False)
        print(a.shape)
        return a

    def walk_to_matrix(self, walks):
        hashed_walks = []
        for j, wa in enumerate(walks):
            d = dict()
            for node, step in wa:
                if node in d:
                    d[node] += self.walk_hash_fn(step)
                else:
                    d[node] = self.walk_hash_fn(step)
            hashed_walks.append(d)
        rows = []
        cols = []
        vals = []
        thresh = self.feature_inclusion_threshold * sum(hashed_walks[0].values())
        for i, h in enumerate(hashed_walks):
            for k in h.keys():
                if h[k] > thresh:
                    rows.append(i)
                    cols.append(k)
                    vals.append(h[k])
        mat = sparse.csr_matrix((vals, (rows, cols)), shape=(len(walks), len(walks)))
        normalize(mat, norm='l1', copy=False)
        return mat

    def generate_indexes(self, mat, noderanks):
        max_filled = self.dense_num_features*len(noderanks)
        y, x = mat.nonzero()
        row_indices = np.array(create_row_indices(y))
        filled = 0
        indexes = []
        for i in noderanks:
            points = np.where(mat[i, :].toarray() > 0)[1]
            filled += nonzero(y, x, row_indices, points)
            if filled < max_filled:
                indexes.append(i)
            elif filled > max_filled:
                break
            else:
                indexes.append(i)
                break
        return indexes
