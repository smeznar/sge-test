from numba import jit, prange
import numpy as np
from scipy.stats import moyal
import scipy.io
import tqdm
import networkx
import itertools
import scipy.sparse
import math
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import chi2, SelectKBest
from collections import Counter
from itertools import chain
import time


def load_matrix(path):
    mat = scipy.io.loadmat(path)

    network = mat['network']  ## omrezje
    labels = mat['group']  ## labele
    return network, labels


@jit(parallel=True, nogil=True, nopython=True)
def numba_walk_kernel(walk_matrix, node_name, sparse_pointers, sparse_neighbors, num_steps=3, num_walks=100):
    for walk in prange(num_walks):
        curr = node_name
        offset = walk * (num_steps + 1)
        walk_matrix[offset] = node_name
        for step in prange(num_steps):
            num_neighs = sparse_pointers[curr+1] - sparse_pointers[curr]
            if num_neighs > 0:
                curr = sparse_neighbors[sparse_pointers[curr] + np.random.randint(num_neighs)]
            idx = offset+step+1
            walk_matrix[idx] = curr


class NodeVectorizer:
    def __init__(self, network, labels, walk_params):
        self.walk_params = walk_params
        self.network = network.tocsr()
        self.labels = labels
        self.sparse_pointers = self.network.indptr
        self.sparse_neighbors = self.network.indices
        self.target_node_map = {}
        self.target_nodes = []
        self.label_vector = []
        self.net = networkx.from_scipy_sparse_matrix(self.network)
        for enx, node in enumerate(self.net.nodes(data=True)):
            self.target_node_map[node[0]] = enx
            self.target_nodes.append(node[0])
        self.final_documents = []

    def label_from_node(self, node_num):
        row = self.labels[node_num]
        return np.where(row == 1)[0][0]

    def sample(self, vectorizer):
        for node in tqdm.tqdm(self.target_nodes):
            #self.label_vector.append(int(node[1]))
            node_words = self.sample_neighborhood(self.target_node_map[node])
            node_words = itertools.chain(*node_words)

            if vectorizer == "binary":
                self.final_documents.append(" ".join(set(node_words)))  ## counts do not matter here.

            elif vectorizer == "rules":
                self.final_documents.append(set(node_words))
            elif vectorizer == "list":
                self.final_documents.append(list(node_words))
            else:
                self.final_documents.append(" ".join(node_words))

    def sample_neighborhood(self, node_name):
        """
        Very fast node sampling.
        """
        walk_struct = []
        for i in range(len(self.walk_params.dist)):
            enx = self.walk_params.steps_low + i
            wlen = self.walk_params.dist[i]
            walk_matrix = -np.ones((wlen, (enx+1)), dtype=np.int32, order='C')
            walk_matrix = np.reshape(walk_matrix, (walk_matrix.size,), order='C')
            numba_walk_kernel(walk_matrix, node_name, self.sparse_pointers, self.sparse_neighbors, num_steps=enx, num_walks=wlen)
            # walk_mat = np.reshape(walk_matrix,(wlen,(enx+1)))[:,1:].flatten()
            if self.walk_params.walk_type == "pattern":
                walk_string = [str(x)+"_"+str(enx) for x in walk_matrix]
            elif self.walk_params.walk_type == "array":
                walk_string = [(x, (j % (enx+1))) for j, x in enumerate(walk_matrix)]
            else:
                walk_string = []
                one_walk = []
                for j in range(len(walk_matrix)):
                    one_walk.append(walk_matrix[j])
                    if (j % (enx+1)) == enx:
                        for k in range(1, enx+2):
                            walk_string.append("_".join([str(n) for n in one_walk[-k:]]))
                        one_walk = []
            walk_struct.append(walk_string)
        return walk_struct

    def vectorize(self, method):
        vec = method.fit(self.final_documents, self.labels)
        return vec


class ImportanceVec:
    def __init__(self, weight_fun):
        self.weight_fun = weight_fun

    def fit(self, data, labels):
        lm = scipy.sparse.lil_matrix((len(data), len(data)), dtype=np.float32)
        for i in tqdm.tqdm(range(len(data))):
            node = data[i]
            for step in node:
                lm[i, step[0]] += self.weight_fun(step[1])
        return lm


class FrequencyVec:
    class VecDict:
        def __init__(self, arr, step_weight, frequency_weight):
            self.d = {}
            self.size = 0
            for node, step in arr:
                if node not in self.d:
                    self.d[node] = step_weight(step)
                else:
                    self.d[node] += step_weight(step)
            m = max(self.d.values())
            for key, value in self.d.items():
                weight = frequency_weight(value/m)
                self.d[key] = weight
                self.size += weight**2
            self.size = math.sqrt(self.size)

        def document_terms(self):
            return list(self.d.keys())

        def reweight(self, scores):
            for k, v in self.d.items():
                self.d[k] = v*scores[k]

        @staticmethod
        def similarity(d1, d2):
            keys = d1.d.keys() & d2.d.keys()
            score = 0
            for k in keys:
                score += d1.d[k]*d2.d[k]
            return score / (d1.size*d2.size)

    def __init__(self, step_fun, neighbourhood_weight_fun, node_weight_fun, num_of_features):
        self.step_weight = step_fun
        self.nh_weight_fun = neighbourhood_weight_fun
        self.node_weight_fun = node_weight_fun
        self.num_of_features = num_of_features

    def fit(self, data, labels):
        print("Walk scoring")
        time.sleep(0.1)
        dictionaries = []
        for wa in tqdm.tqdm(data):
            dictionaries.append(self.VecDict(wa, self.step_weight, self.nh_weight_fun))
        print("Reweight")
        time.sleep(0.1)
        node_freq = Counter(chain.from_iterable([d.document_terms() for d in dictionaries]))
        node_weight = self.node_weight_fun(node_freq, len(data))
        for d in tqdm.tqdm(dictionaries):
            d.reweight(node_weight)
        print("Feature scoring")
        time.sleep(0.1)
        a = DictVectorizer(sparse=True)
        b = a.fit_transform([d.d for d in dictionaries])
        sb = SelectKBest(chi2, self.num_of_features).fit(b, labels)
        features = np.where(sb.get_support())[0].tolist()
        print("Feature creation")
        time.sleep(0.1)
        feature_mat = []
        for d in tqdm.tqdm(dictionaries):
            vec = []
            for i in features:
                vec.append(self.VecDict.similarity(d, dictionaries[i]))
            feature_mat.append(vec)
        return np.array(feature_mat)


class WalkParams:
    def __init__(self, steps_l=1, steps_h=5, dist_name="uniform", dist=[], num_walks=1000, walk_type="array"):
        self.steps_low = steps_l
        self.steps_high = steps_h
        self.num_walks = num_walks
        self.walk_type = walk_type
        if dist_name == "custom" and len(dist)==(steps_h-steps_l+1):
            self.dist = dist
        elif dist_name == "custom":
            self.dist = self.get_dist("uniform", steps_h-steps_l+1)
        else:
            self.dist = self.get_dist(dist_name, steps_h-steps_l+1)

    def get_dist(self, dist_name, walk_len):
        if dist_name == "moyal":
            r = moyal.rvs(size=self.num_walks)
            inds = np.histogram(r, walk_len)
        elif dist_name == "levy":
            r = moyal.rvs(size=self.num_walks)
            inds = np.histogram(r, walk_len)
        elif dist_name == "uniform":
            r = np.random.uniform(0, 1, self.num_walks)
            inds = np.histogram(r, walk_len)
        else:
            pass
        return inds[0]


def tf_idf_doc_weigh(document_freq: Counter, doc_num: int):
    return {c: 1 for c in list(document_freq)}


if __name__ == '__main__':
    net, lab = load_matrix("../benchmark_toolkit/datasets/citeseer.mat")
    walk_params = WalkParams()
    a = NodeVectorizer(net, lab, walk_params)
    a.sample("list")
    b = a.vectorize(FrequencyVec(lambda a: 1/(a+1), lambda x: np.log2(1+x), tf_idf_doc_weigh, 2500))
    np.savetxt("../embeddings/cite_test3.csv", b)
    pass
